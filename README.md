# Generator-draft005
[![Build Status](https://secure.travis-ci.org/oxmo456/generator-draft005.png?branch=master)](https://travis-ci.org/oxmo456/generator-draft005)

A generator for Yeoman.

## Getting started
- Make sure you have [yo](https://github.com/yeoman/yo) installed:
    `npm install -g yo`
- Install the generator: `npm install -g generator-draft005`
- Run: `yo draft005`

## License
[MIT License](http://en.wikipedia.org/wiki/MIT_License)
